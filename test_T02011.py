# -*- coding: utf-8 -*-
import selenium
from selenium.webdriver.common.by import By
import selenium.webdriver.common.keys
import selenium.webdriver.support.ui
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest, time, re

class test_T02011(unittest.TestCase):
    def setUp(self):
        self.driver = selenium.webdriver.Chrome('chromedriver')
        self.driver.implicitly_wait(30)
        # self.base_url = "https://www.katalon.com/"
        self.base_url = "https://wipm-s.trialpack.net"
        self.verificationErrors = []
        self.accept_next_alert = True
    
    def test_t02011(self):
        driver = self.driver
        # driver.get("https://accounts-s.eoffice.openlab.tw/cas/login?service=https%3A%2F%2Fwipm-s.eoffice.trialpack.net%2F")
        driver.get("https://wipm-s.trialpack.net")
        time.sleep(3)
        driver.find_element_by_id("username").click()
        driver.find_element_by_id("username").clear()
        driver.find_element_by_id("username").send_keys("9811039")
        driver.find_element_by_id("password").click()
        driver.find_element_by_id("password").clear()
        driver.find_element_by_id("password").send_keys("WiG4fAb8Y(")
        driver.find_element_by_id("loginbtn").click()
        driver.find_element_by_id("monthly_cost0").click()
        driver.find_element_by_id("monthly_cost0").clear()
        driver.find_element_by_id("monthly_cost0").send_keys("100000qwwe")
        driver.find_element_by_id("monthly_cost1").click()
        driver.find_element_by_id("monthly_cost1").clear()
        driver.find_element_by_id("monthly_cost1").send_keys("100000vfs")
        driver.find_element_by_id("monthly_cost2").click()
        driver.find_element_by_id("monthly_cost2").clear()
        driver.find_element_by_id("monthly_cost2").send_keys("14000gsd")
        driver.find_element_by_id("monthly_cost3").click()
        driver.find_element_by_id("monthly_cost3").clear()
        driver.find_element_by_id("monthly_cost3").send_keys("14000rewrt")
        driver.find_element_by_id("SaveBtn").click()
        time.sleep(5)
        #driver.find_element_by_xpath("//li[9]/a/p").click()
        driver.find_element_by_xpath("(.//*[normalize-space(text()) and normalize-space(.)='(107)'])[1]/following::p[1]").click()
        time.sleep(3)
    
    def is_element_present(self, how, what):
        try: self.driver.find_element(by=how, value=what)
        except NoSuchElementException as e: return False
        return True
    
    def is_alert_present(self):
        try: self.driver.switch_to_alert()
        except NoAlertPresentException as e: return False
        return True
    
    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally: self.accept_next_alert = True
    
    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()
