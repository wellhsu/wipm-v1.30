# -*- coding: utf-8 -*-
import selenium
from selenium.webdriver.common.by import By
import selenium.webdriver.common.keys
from selenium.common.exceptions import NoAlertPresentException
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.support.ui import Select
import unittest, time, re

class test_T02074(unittest.TestCase):
    def setUp(self):
        self.driver = selenium.webdriver.Chrome('chromedriver')
        self.driver.implicitly_wait(30)
        # self.base_url = "https://www.katalon.com/"
        self.base_url = "https://wipm-s.trialpack.net"
        self.verificationErrors = []
        self.accept_next_alert = True

    def test_t02074(self):
        driver = self.driver
        # driver.get("https://accounts-s.eoffice.openlab.tw/cas/login?service=https%3A%2F%2Fwipm-s.eoffice.trialpack.net%2F")
        driver.get("https://wipm-s.trialpack.net")
        time.sleep(3)
        driver.find_element_by_id("username").click()
        driver.find_element_by_id("username").clear()
        driver.find_element_by_id("username").send_keys("9811039")
        driver.find_element_by_id("password").click()
        driver.find_element_by_id("password").clear()
        driver.find_element_by_id("password").send_keys("WiG4fAb8Y(")
        driver.find_element_by_id("loginbtn").click()
        #driver.find_element_by_xpath("//b").click()
        #driver.find_element_by_xpath(u"(.//*[normalize-space(text()) and normalize-space(.)='待簽核表單'])[1]/preceding::p[8]").click()
        driver.find_element_by_xpath(u"(.//*[normalize-space(text()) and normalize-space(.)='待簽核表單'])[1]/preceding::b[2]").click()
        driver.find_element_by_xpath(u"(.//*[normalize-space(text()) and normalize-space(.)='待簽核表單'])[1]/preceding::p[8]").click()
        driver.find_element_by_id("mplid").click()
        driver.find_element_by_id("mplid").clear()
        driver.find_element_by_id("mplid").send_keys("9308005")
        driver.find_element_by_id("SaveBtn").click()
        driver.find_element_by_xpath("//span[2]/span").click()
        driver.find_element_by_xpath(u"(.//*[normalize-space(text()) and normalize-space(.)='人力收費表維護'])[2]/following::span[6]").click()
        Select(driver.find_element_by_id("SelectYear")).select_by_visible_text("2018")
        time.sleep(3)
        driver.find_element_by_xpath("//div[2]/button/span[2]/span").click()
        driver.find_element_by_link_text("11").click()
        Select(driver.find_element_by_id("SelectMonth")).select_by_visible_text("11")
        driver.find_element_by_xpath("//span[2]/span").click()
        driver.find_element_by_xpath(u"(.//*[normalize-space(text()) and normalize-space(.)='人力收費表維護'])[2]/following::span[8]").click()
        driver.find_element_by_xpath(u"(.//*[normalize-space(text()) and normalize-space(.)='金額'])[1]/following::button[1]").click()
        driver.find_element_by_link_text("TWD").click()
        time.sleep(3)
        driver.find_element_by_link_text("CNY").click()
        time.sleep(3)
        driver.find_element_by_id("monthly_cost0").click()
        time.sleep(3)
        driver.find_element_by_id("monthly_cost1").click()
        time.sleep(3)
        driver.find_element_by_id("monthly_cost2").click()
        time.sleep(3)
        driver.find_element_by_id("monthly_cost3").click()
        time.sleep(3)
        driver.find_element_by_xpath("(.//*[normalize-space(text()) and normalize-space(.)='(107)'])[1]/following::p[1]").click()
        time.sleep(3)
    
    def is_element_present(self, how, what):
        try: self.driver.find_element(by=how, value=what)
        except NoSuchElementException as e: return False
        return True
    
    def is_alert_present(self):
        try: self.driver.switch_to_alert()
        except NoAlertPresentException as e: return False
        return True
    
    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally: self.accept_next_alert = True
    
    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()
