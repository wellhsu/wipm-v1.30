# -*- coding: utf-8 -*-
import time
import unittest
import selenium
import selenium.webdriver.common.keys
from selenium.common.exceptions import NoAlertPresentException
from selenium.common.exceptions import NoSuchElementException

class TestT02024(unittest.TestCase):
    def setUp(self):
        self.driver = selenium.webdriver.Chrome('chromedriver')
        self.driver.implicitly_wait(30)
        # self.base_url = "https://www.katalon.com/"
        self.base_url = "https://wipm-s.trialpack.net"
        self.verificationErrors = []
        self.accept_next_alert = True
    
    def test_t02024(self):
        driver = self.driver
        driver.get("https://wipm-s.trialpack.net")
        time.sleep(3)
        driver.find_element_by_id("username").clear()
        driver.find_element_by_id("username").send_keys("9811039")
        driver.find_element_by_id("password").clear()
        driver.find_element_by_id("password").send_keys("WiG4fAb8Y(")
        driver.find_element_by_id("loginbtn").click()
        #driver.find_element_by_xpath(u"(.//*[normalize-space(text()) and normalize-space(.)='人力收費表維護'])[2]/preceding::b[2]").click()
        #driver.find_element_by_xpath(u"(.//*[normalize-space(text()) and normalize-space(.)='人力收費表維護'])[2]/preceding::p[11]").click()
        driver.find_element_by_xpath(u"(.//*[normalize-space(text()) and normalize-space(.)='待簽核表單'])[1]/preceding::b[2]").click()
        driver.find_element_by_xpath(u"(.//*[normalize-space(text()) and normalize-space(.)='待簽核表單'])[1]/preceding::p[8]").click()
        driver.find_element_by_id("mplid").click()
        driver.find_element_by_id("mplid").clear()
        driver.find_element_by_id("mplid").send_keys("10304048")
        driver.find_element_by_id("date").click()
        driver.find_element_by_id("date").clear()
        driver.find_element_by_id("date").send_keys("2018/07/07")
        driver.find_element_by_id("SaveBtn").click()
        time.sleep(3)
        #driver.find_element_by_xpath(u"(.//*[normalize-space(text()) and normalize-space(.)='登入失敗'])[1]/preceding::p[1]").click()
        #driver.find_element_by_id("username").clear()
        #driver.find_element_by_id("username").send_keys("9811039")
        #driver.find_element_by_id("password").clear()
        #driver.find_element_by_id("password").send_keys("G4fAb8Y@")
        #driver.find_element_by_id("loginbtn").click()
        # driver.find_element_by_xpath(u"(.//*[normalize-space(text()) and normalize-space(.)='人力收費表維護'])[2]/preceding::b[2]").click()
        # driver.find_element_by_xpath(u"(.//*[normalize-space(text()) and normalize-space(.)='人力收費表維護'])[2]/preceding::p[11]").click()
        #driver.find_element_by_xpath(u"(.//*[normalize-space(text()) and normalize-space(.)='待簽核表單'])[1]/preceding::p[11]").click()
        #driver.find_element_by_xpath(u"(.//*[normalize-space(text()) and normalize-space(.)='待簽核表單'])[1]/preceding::p[8]").click()
        #driver.find_element_by_id("mplid").click()
        #driver.find_element_by_id("mplid").clear()
        #driver.find_element_by_id("mplid").send_keys("10304048")
        driver.find_element_by_id("date").click()
        driver.find_element_by_id("date").clear()
        driver.find_element_by_id("date").send_keys("2018.07.07")
        driver.find_element_by_id("SaveBtn").click()
        time.sleep(3)
        driver.find_element_by_xpath(u"(.//*[normalize-space(text()) and normalize-space(.)='待簽核表單'])[1]/following::p[1]").click()
        #driver.find_element_by_id("username").clear()
        #driver.find_element_by_id("username").send_keys("9811039")
        #driver.find_element_by_id("password").clear()
        #driver.find_element_by_id("password").send_keys("G4fAb8Y@")
        #driver.find_element_by_id("loginbtn").click()
        #driver.find_element_by_xpath(u"(.//*[normalize-space(text()) and normalize-space(.)='人力收費表維護'])[2]/preceding::b[2]").click()
        #driver.find_element_by_xpath(u"(.//*[normalize-space(text()) and normalize-space(.)='人力收費表維護'])[2]/preceding::p[11]").click()
        #driver.find_element_by_xpath(u"(.//*[normalize-space(text()) and normalize-space(.)='待簽核表單'])[1]/preceding::b[2]").click()
        #driver.find_element_by_xpath(u"(.//*[normalize-space(text()) and normalize-space(.)='待簽核表單'])[1]/preceding::p[8]").click()
        #driver.find_element_by_id("mplid").click()
        #driver.find_element_by_id("mplid").clear()
        #driver.find_element_by_id("mplid").send_keys("10304048")
        #driver.find_element_by_id("date").click()
        #driver.find_element_by_id("date").clear()
        #driver.find_element_by_id("date").send_keys("2018-07-07")
        #driver.find_element_by_id("SaveBtn").click()
        #time.sleep(5)
        #driver.find_element_by_xpath(u"(.//*[normalize-space(text()) and normalize-space(.)='績效表'])[2]/preceding::p[2]").click()
        time.sleep(3)
    
    def is_element_present(self, how, what):
        try: self.driver.find_element(by=how, value=what)
        except NoSuchElementException as e: return False
        return True
    
    def is_alert_present(self):
        try: self.driver.switch_to_alert()
        except NoAlertPresentException as e: return False
        return True
    
    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally: self.accept_next_alert = True
    
    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()
