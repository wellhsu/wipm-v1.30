# -*- coding: utf-8 -*-
import selenium
import selenium.webdriver.common.by
import selenium.webdriver.common.keys
import selenium.webdriver.support.ui
import selenium.webdriver.common.keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
import selenium.common.exceptions
import unittest, time, re


class test_T02008(unittest.TestCase):
    def setUp(self):
        self.driver = selenium.webdriver.Chrome('chromedriver')
        self.driver.implicitly_wait(30)
        # self.base_url = "https://www.katalon.com/"
        self.base_url = "https://wipm-s.trialpack.net"
        self.verificationErrors = []
        self.accept_next_alert = True
    
    def test_t02008(self):
        driver = self.driver
        # driver.get("https://accounts-s.eoffice.openlab.tw/cas/login?service=https%3A%2F%2Fwipm-s.eoffice.trialpack.net%2F")
        driver.get("https://wipm-s.trialpack.net")
        time.sleep(3)
        driver.find_element_by_id("username").click()
        driver.find_element_by_id("username").clear()
        driver.find_element_by_id("username").send_keys("9811039")
        driver.find_element_by_id("password").click()
        driver.find_element_by_id("password").clear()
        driver.find_element_by_id("password").send_keys("WiG4fAb8Y(")
        driver.find_element_by_id("loginbtn").click()
        driver.find_element_by_xpath("//tr[@id='addr0']/td/div/button/span").click()
        driver.find_element_by_link_text("CNY").click()
        Select(driver.find_element_by_id("currency0")).select_by_visible_text("CNY")
        driver.find_element_by_xpath("//tr[@id='addr1']/td/div/button/span").click()
        driver.find_element_by_xpath("//tr[@id='addr1']/td/div/div/ul/li[2]/a").click()
        Select(driver.find_element_by_id("currency1")).select_by_visible_text("CNY")
        driver.find_element_by_xpath("//tr[@id='addr2']/td/div/button/span").click()
        driver.find_element_by_xpath("//tr[@id='addr2']/td/div/div/ul/li/a").click()
        Select(driver.find_element_by_id("currency2")).select_by_visible_text("TWD")
        driver.find_element_by_xpath("//tr[@id='addr3']/td/div/button/span").click()
        driver.find_element_by_xpath("//tr[@id='addr3']/td/div/div/ul/li/a").click()
        Select(driver.find_element_by_id("currency3")).select_by_visible_text("TWD")
        driver.find_element_by_id("monthly_cost0").click()
        driver.find_element_by_id("monthly_cost0").clear()
        driver.find_element_by_id("monthly_cost0").send_keys("100000~!@#")
        driver.find_element_by_id("monthly_cost1").click()
        driver.find_element_by_id("monthly_cost1").clear()
        driver.find_element_by_id("monthly_cost1").send_keys("100000$%^&")
        driver.find_element_by_id("monthly_cost2").click()
        driver.find_element_by_id("monthly_cost2").clear()
        driver.find_element_by_id("monthly_cost2").send_keys("14000*()_+")
        driver.find_element_by_id("monthly_cost3").click()
        driver.find_element_by_id("monthly_cost3").clear()
        driver.find_element_by_id("monthly_cost3").send_keys("14000|}{\":")
        driver.find_element_by_id("monthly_cost2").click()
        driver.find_element_by_id("monthly_cost0").click()
        driver.find_element_by_id("monthly_cost0").clear()
        driver.find_element_by_id("monthly_cost0").send_keys("100000~!@4")
        driver.find_element_by_id("monthly_cost1").click()
        driver.find_element_by_id("monthly_cost1").clear()
        driver.find_element_by_id("monthly_cost1").send_keys("100000$%^.")
        #driver.find_element_by_xpath("(//button[@type='button'])[5]").click()
        driver.find_element_by_xpath("(.//*[normalize-space(text()) and normalize-space(.)='WSH'])[1]/following::button[1]").click()
        time.sleep(5)
        #driver.find_element_by_xpath("//li[9]/a/p").click()
        driver.find_element_by_xpath("(.//*[normalize-space(text()) and normalize-space(.)='(107)'])[1]/following::p[1]").click()
        time.sleep(3)
    
    def is_element_present(self, how, what):
        try: self.driver.find_element(by=how, value=what)
        except NoSuchElementException as e: return False
        return True
    
    def is_alert_present(self):
        try: self.driver.switch_to_alert()
        except selenium.common.exceptions.NoAlertPresentException as e: return False
        return True
    
    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally: self.accept_next_alert = True
    
    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()
