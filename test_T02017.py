# -*- coding: utf-8 -*-
import time
import unittest
import selenium
import selenium.webdriver.common.keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoAlertPresentException
from selenium.common.exceptions import NoSuchElementException

class TestT02017(unittest.TestCase):
    def setUp(self):
        self.driver = selenium.webdriver.Chrome('chromedriver')
        self.driver.implicitly_wait(30)
        # self.base_url = "https://www.katalon.com/"
        self.base_url = "https://wipm-s.trialpack.net"
        self.verificationErrors = []
        self.accept_next_alert = True
    
    def test_t02017(self):
        driver = self.driver
        driver.get("https://wipm-s.trialpack.net")
        time.sleep(3)
        driver.find_element_by_id("username").click()
        driver.find_element_by_id("username").clear()
        driver.find_element_by_id("username").send_keys("9811039")
        driver.find_element_by_id("password").click()
        driver.find_element_by_id("password").clear()
        driver.find_element_by_id("password").send_keys("WiG4fAb8Y(")
        driver.find_element_by_id("loginbtn").click()
        driver.find_element_by_xpath("//p").click()
        driver.find_element_by_xpath("//li[2]/a/p").click()
        driver.find_element_by_xpath("//button/span").click()
        driver.find_element_by_link_text("2020").click()
        Select(driver.find_element_by_id("SelectYear")).select_by_visible_text("2020")
        driver.find_element_by_xpath("//tr[@id='addr0']/td[2]").click()
        driver.find_element_by_id("to_ntd0").click()
        driver.find_element_by_id("to_ntd0").click()
        driver.find_element_by_id("to_ntd0").clear()
        driver.find_element_by_id("to_ntd0").send_keys("3453465")
        driver.find_element_by_id("to_ntd1").click()
        driver.find_element_by_id("to_ntd1").clear()
        driver.find_element_by_id("to_ntd1").send_keys("gewrtgewtq")
        driver.find_element_by_id("to_ntd2").click()
        driver.find_element_by_id("to_ntd2").clear()
        driver.find_element_by_id("to_ntd2").send_keys("[0-['['p")
        driver.find_element_by_id("to_ntd3").click()
        driver.find_element_by_id("to_ntd3").clear()
        driver.find_element_by_id("to_ntd3").send_keys(".lkjlk.gkj")
        driver.find_element_by_id("to_ntd4").click()
        driver.find_element_by_id("to_ntd4").clear()
        driver.find_element_by_id("to_ntd4").send_keys(".u876")
        driver.find_element_by_id("to_ntd5").click()
        driver.find_element_by_id("to_ntd5").clear()
        driver.find_element_by_id("to_ntd5").send_keys("432676ikjr")
        driver.find_element_by_id("to_ntd6").click()
        driver.find_element_by_id("to_ntd6").clear()
        driver.find_element_by_id("to_ntd6").send_keys("eqyi6nenye")
        driver.find_element_by_xpath("//tr[@id='addr7']/td[2]").click()
        driver.find_element_by_id("to_ntd7").click()
        driver.find_element_by_id("to_ntd7").clear()
        driver.find_element_by_id("to_ntd7").send_keys("wt5fewe")
        driver.find_element_by_id("to_ntd8").click()
        driver.find_element_by_id("to_ntd8").clear()
        driver.find_element_by_id("to_ntd8").send_keys("4321454326")
        driver.find_element_by_id("to_ntd9").click()
        driver.find_element_by_id("to_ntd9").clear()
        driver.find_element_by_id("to_ntd9").send_keys("7456.4356")
        driver.find_element_by_id("to_ntd10").click()
        driver.find_element_by_id("to_ntd10").clear()
        driver.find_element_by_id("to_ntd10").send_keys("4574576")
        driver.find_element_by_id("to_ntd11").click()
        driver.find_element_by_id("to_ntd11").clear()
        driver.find_element_by_id("to_ntd11").send_keys("mnbv")
        time.sleep(5)
        driver.find_element_by_xpath("(//button[@type='button'])[3]").click()
        driver.find_element_by_xpath("//li[9]/a/p").click()
        time.sleep(3)
    
    def is_element_present(self, how, what):
        try: self.driver.find_element(by=how, value=what)
        except NoSuchElementException as e: return False
        return True
    
    def is_alert_present(self):
        try: self.driver.switch_to_alert()
        except NoAlertPresentException as e: return False
        return True
    
    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally: self.accept_next_alert = True
    
    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()
