# -*- coding: utf-8 -*-
import time
import unittest
import selenium
import selenium.webdriver.common.keys
from selenium.common.exceptions import NoAlertPresentException
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.support.ui import Select


class TestT02033(unittest.TestCase):
    def setUp(self):
        self.driver = selenium.webdriver.Chrome('chromedriver')
        self.driver.implicitly_wait(30)
        # self.base_url = "https://www.katalon.com/"
        self.base_url = "https://wipm-s.trialpack.net"
        self.verificationErrors = []
        self.accept_next_alert = True
    
    def test_t02033(self):
        driver = self.driver
        driver.get("https://wipm-s.trialpack.net")
        time.sleep(3)
        driver.find_element_by_id("username").clear()
        driver.find_element_by_id("username").send_keys("9811039")
        driver.find_element_by_id("password").clear()
        driver.find_element_by_id("password").send_keys("WiG4fAb8Y(")
        driver.find_element_by_id("loginbtn").click()
        driver.find_element_by_xpath(u"(.//*[normalize-space(text()) and normalize-space(.)='待簽核表單'])[1]/preceding::b[1]").click()
        driver.find_element_by_xpath(u"(.//*[normalize-space(text()) and normalize-space(.)='待簽核表單'])[1]/preceding::p[4]").click()
        #driver.find_element_by_xpath(u"(.//*[normalize-space(text()) and normalize-space(.)='待簽核表單'])[1]/preceding::b[1]").click()
        #driver.find_element_by_xpath(u"(.//*[normalize-space(text()) and normalize-space(.)='待簽核表單'])[1]/preceding::p[4]").click()
        driver.find_element_by_xpath("(.//*[normalize-space(text()) and normalize-space(.)='MR0000'])[1]/following::span[2]").click()
        #driver.find_element_by_xpath("(.//*[normalize-space(text()) and normalize-space(.)='MR0A00'])[1]/following::span[2]").click()
        Select(driver.find_element_by_id("SelectDep")).select_by_visible_text("MR0A10")
        time.sleep(3)
        driver.find_element_by_xpath("(.//*[normalize-space(text()) and normalize-space(.)='MR0A10'])[1]/following::span[2]").click()
        driver.find_element_by_xpath("(.//*[normalize-space(text()) and normalize-space(.)='MR0A10'])[2]/following::span[2]").click()
        Select(driver.find_element_by_id("SelectDep")).select_by_visible_text("MR0A30")
        time.sleep(3)
        driver.find_element_by_xpath("(.//*[normalize-space(text()) and normalize-space(.)='MR0A30'])[1]/following::span[2]").click()
        driver.find_element_by_xpath("(.//*[normalize-space(text()) and normalize-space(.)='MR0A30'])[1]/following::span[2]").click()
        Select(driver.find_element_by_id("SelectDep")).select_by_visible_text("MR0M00")
        time.sleep(3)
        driver.find_element_by_xpath("(.//*[normalize-space(text()) and normalize-space(.)='MR0M00'])[1]/following::span[2]").click()
        driver.find_element_by_xpath("(.//*[normalize-space(text()) and normalize-space(.)='MR0P10'])[1]/following::span[2]").click()
        Select(driver.find_element_by_id("SelectDep")).select_by_visible_text("MR0P30")
        time.sleep(3)
        driver.find_element_by_xpath("(.//*[normalize-space(text()) and normalize-space(.)='MR0P30'])[1]/following::span[2]").click()
        driver.find_element_by_xpath("(.//*[normalize-space(text()) and normalize-space(.)='MR0P30'])[2]/following::span[2]").click()
        Select(driver.find_element_by_id("SelectDep")).select_by_visible_text("MR0Q00")
        time.sleep(3)
        driver.find_element_by_xpath(u"(.//*[normalize-space(text()) and normalize-space(.)='部門人員查詢'])[2]/following::button[1]").click()
        driver.find_element_by_xpath("(.//*[normalize-space(text()) and normalize-space(.)='MR0U20'])[1]/following::span[2]").click()
        Select(driver.find_element_by_id("SelectDep")).select_by_visible_text("MR0U30")
        time.sleep(3)
        driver.find_element_by_xpath(u"(.//*[normalize-space(text()) and normalize-space(.)='部門人員查詢'])[2]/following::button[1]").click()
        driver.find_element_by_xpath("(.//*[normalize-space(text()) and normalize-space(.)='MR0U30'])[2]/following::span[2]").click()
        Select(driver.find_element_by_id("SelectDep")).select_by_visible_text("MR0V10")
        time.sleep(3)
        driver.find_element_by_xpath(u"(.//*[normalize-space(text()) and normalize-space(.)='部門人員查詢'])[2]/following::button[1]").click()
        driver.find_element_by_xpath("(.//*[normalize-space(text()) and normalize-space(.)='MR0Q00'])[1]/following::span[2]").click()
        Select(driver.find_element_by_id("SelectDep")).select_by_visible_text("MR0S00")
        time.sleep(3)
        driver.find_element_by_xpath(u"(.//*[normalize-space(text()) and normalize-space(.)='部門人員查詢'])[2]/following::button[1]").click()
        driver.find_element_by_xpath("(.//*[normalize-space(text()) and normalize-space(.)='MRD000'])[1]/following::span[2]").click()
        Select(driver.find_element_by_id("SelectDep")).select_by_visible_text("MRD100")
        time.sleep(3)
        driver.find_element_by_xpath("(.//*[normalize-space(text()) and normalize-space(.)='MRD100'])[1]/following::span[2]").click()
        driver.find_element_by_xpath("(.//*[normalize-space(text()) and normalize-space(.)='MRE200'])[1]/following::span[2]").click()
        Select(driver.find_element_by_id("SelectDep")).select_by_visible_text("MRE300")
        time.sleep(3)
        driver.find_element_by_xpath(u"(.//*[normalize-space(text()) and normalize-space(.)='部門人員查詢'])[2]/following::button[1]").click()
        driver.find_element_by_xpath("(.//*[normalize-space(text()) and normalize-space(.)='MRK600'])[1]/following::span[2]").click()
        Select(driver.find_element_by_id("SelectDep")).select_by_visible_text("MRK700")
        time.sleep(3)
        driver.find_element_by_xpath("(.//*[normalize-space(text()) and normalize-space(.)='MRK700'])[1]/following::span[2]").click()
        driver.find_element_by_link_text("MRP300").click()
        Select(driver.find_element_by_id("SelectDep")).select_by_visible_text("MRP300")
        time.sleep(3)
        driver.find_element_by_xpath("(.//*[normalize-space(text()) and normalize-space(.)='MRP300'])[1]/following::span[2]").click()
        driver.find_element_by_link_text("MRS100").click()
        Select(driver.find_element_by_id("SelectDep")).select_by_visible_text("MRS100")
        time.sleep(3)
        driver.find_element_by_xpath(u"(.//*[normalize-space(text()) and normalize-space(.)='部門人員查詢'])[2]/following::button[1]").click()
        driver.find_element_by_xpath("(.//*[normalize-space(text()) and normalize-space(.)='MRT200'])[1]/following::span[2]").click()
        Select(driver.find_element_by_id("SelectDep")).select_by_visible_text("MRT300")
        time.sleep(3)
        driver.find_element_by_xpath("(.//*[normalize-space(text()) and normalize-space(.)='MRT300'])[1]/following::span[2]").click()
        driver.find_element_by_xpath("(.//*[normalize-space(text()) and normalize-space(.)='MRT600'])[1]/following::span[2]").click()
        Select(driver.find_element_by_id("SelectDep")).select_by_visible_text("MRW000")
        time.sleep(3)
        driver.find_element_by_xpath("(.//*[normalize-space(text()) and normalize-space(.)='MRW000'])[1]/following::span[2]").click()
        driver.find_element_by_xpath("(.//*[normalize-space(text()) and normalize-space(.)='MRW000'])[2]/following::span[2]").click()
        Select(driver.find_element_by_id("SelectDep")).select_by_visible_text("MRWH00")
        time.sleep(3)
        driver.find_element_by_xpath(u"(.//*[normalize-space(text()) and normalize-space(.)='部門人員查詢'])[2]/following::button[1]").click()
        driver.find_element_by_xpath("(.//*[normalize-space(text()) and normalize-space(.)='MRT000'])[1]/following::span[2]").click()
        Select(driver.find_element_by_id("SelectDep")).select_by_visible_text("MRT100")
        time.sleep(3)
        driver.find_element_by_xpath(u"(.//*[normalize-space(text()) and normalize-space(.)='待簽核表單'])[1]/following::p[1]").click()
        #driver.find_element_by_xpath("(.//*[normalize-space(text()) and normalize-space(.)='(2)'])[1]/following::p[1]").click()
        time.sleep(3)
        #driver.find_element_by_xpath(u"(.//*[normalize-space(text()) and normalize-space(.)='人力收費表維護'])[2]/preceding::b[1]").click()
        #driver.find_element_by_xpath(u"(.//*[normalize-space(text()) and normalize-space(.)='人力收費表維護'])[2]/preceding::p[7]").click()
        #driver.find_element_by_xpath(u"(.//*[normalize-space(text()) and normalize-space(.)='待簽核表單'])[1]/preceding::b[1]").click()
        #driver.find_element_by_xpath(u"(.//*[normalize-space(text()) and normalize-space(.)='待簽核表單'])[1]/preceding::p[4]").click()
        #driver.find_element_by_xpath("(.//*[normalize-space(text()) and normalize-space(.)='MR0000'])[1]/following::span[2]").click()
        #time.sleep(3)
        #driver.find_element_by_xpath("(.//*[normalize-space(text()) and normalize-space(.)='MR0A00'])[1]/following::span[2]").click()
        #Select(driver.find_element_by_id("SelectDep")).select_by_visible_text("MR0A10")
        #time.sleep(3)
        #driver.find_element_by_xpath(u"(.//*[normalize-space(text()) and normalize-space(.)='部門人員查詢'])[2]/following::button[1]").click()
        #driver.find_element_by_xpath("(.//*[normalize-space(text()) and normalize-space(.)='MR0A30'])[1]/following::span[2]").click()
        #Select(driver.find_element_by_id("SelectDep")).select_by_visible_text("MR0M00")
        #time.sleep(3)
        #driver.find_element_by_xpath("(.//*[normalize-space(text()) and normalize-space(.)='MR0M00'])[1]/following::span[2]").click()
        #driver.find_element_by_link_text("MR0S00").click()
        #Select(driver.find_element_by_id("SelectDep")).select_by_visible_text("MR0S00")
        #time.sleep(3)
        #driver.find_element_by_xpath("(.//*[normalize-space(text()) and normalize-space(.)='MR0S00'])[1]/following::span[2]").click()
        #driver.find_element_by_xpath("(.//*[normalize-space(text()) and normalize-space(.)='MR0U20'])[1]/following::span[2]").click()
        #Select(driver.find_element_by_id("SelectDep")).select_by_visible_text("MR0V00")
        #time.sleep(3)
        #driver.find_element_by_xpath("(.//*[normalize-space(text()) and normalize-space(.)='MR0V00'])[1]/following::span[2]").click()
        #driver.find_element_by_xpath("(.//*[normalize-space(text()) and normalize-space(.)='MR0V00'])[2]/following::span[2]").click()
        #Select(driver.find_element_by_id("SelectDep")).select_by_visible_text("MR0V10")
        #time.sleep(3)
        #driver.find_element_by_xpath("(.//*[normalize-space(text()) and normalize-space(.)='MR0V10'])[1]/following::span[2]").click()
        #driver.find_element_by_link_text("MRD300").click()
        #Select(driver.find_element_by_id("SelectDep")).select_by_visible_text("MRD300")
        #time.sleep(3)
        #driver.find_element_by_xpath("(.//*[normalize-space(text()) and normalize-space(.)='MRD300'])[1]/following::span[2]").click()
        #driver.find_element_by_xpath("(.//*[normalize-space(text()) and normalize-space(.)='MRE200'])[1]/following::span[2]").click()
        #Select(driver.find_element_by_id("SelectDep")).select_by_visible_text("MRE300")
        #time.sleep(3)
        #driver.find_element_by_xpath(u"(.//*[normalize-space(text()) and normalize-space(.)='部門人員查詢'])[2]/preceding::p[2]").click()
        #time.sleep(3)
    
    def is_element_present(self, how, what):
        try: self.driver.find_element(by=how, value=what)
        except NoSuchElementException as e: return False
        return True
    
    def is_alert_present(self):
        try: self.driver.switch_to_alert()
        except NoAlertPresentException as e: return False
        return True
    
    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally: self.accept_next_alert = True
    
    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()
